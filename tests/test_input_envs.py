"""
Copyright (C) 2022 - 2024 J. S. Grewal <rg_public@proton.me>

Licence:                AGPL-3.0-or-later (https://www.gnu.org/licenses/agpl-3.0.en.html)

Codebase:               https://codeberg.org/raja-grewal

Title:                  test_input_envs.py

Description:
    Responsible for conducting tests on user inputs across all custom created
    environments.
"""

import sys

sys.path.append("./")

# default assertion errors
tb: str = "variable must be of type bool"
td: str = "variable must be of type dict"
tf: str = "variable must be of type float"
tfi: str = "variable must be of type float for int"
ti: str = "variable must be of type int"
tl: str = "variable must be of type list"
ts: str = "variable must be of type str"
gte0: str = "quantity must be greater than or equal to 0"
gt0: str = "quantity must be greater than 0"
gte1: str = "quantity must be greater than or equal to 1"


def multi_env_tests(
    name: str,
    MAX_VALUE: float,
    INITIAL_PRICE: float,
    INITIAL_VALUE: float,
    MIN_VALUE_RATIO: float,
    MAX_VALUE_RATIO: float,
    DISCRETE_RETURNS: bool,
    MAX_ABS_ACTION: float,
    MIN_REWARD: float,
    MIN_RETURN: float,
    MAX_RETURN: float,
    MIN_WEIGHT: float,
) -> None:
    """
    Conduct tests for multiplicative environment initialisation.

    Parameters:
        Refer to financial environments in `./envs` for input details.
    """
    assert isinstance(name, str), ts

    assert isinstance(MAX_VALUE, (float, int)), tfi
    assert (
        0 < MAX_VALUE < 1e37
    ), "MAX_VALUE must be less than float32 (single-precision) limit 1e38"
    assert isinstance(INITIAL_PRICE, (float, int)), tfi
    assert INITIAL_PRICE > 0
    assert isinstance(INITIAL_VALUE, (float, int)), tfi
    assert MAX_VALUE > INITIAL_VALUE > 0
    assert isinstance(MIN_VALUE_RATIO, (float, int)), tfi
    assert 1 / MAX_VALUE <= MIN_VALUE_RATIO < 1
    assert isinstance(MAX_VALUE_RATIO, (float, int)), tfi
    assert MIN_VALUE_RATIO < MAX_VALUE_RATIO

    assert isinstance(DISCRETE_RETURNS, bool), tb

    assert isinstance(MAX_ABS_ACTION, (float, int)), tfi
    assert MAX_ABS_ACTION > 0
    assert isinstance(MIN_REWARD, (float, int)), tfi
    assert isinstance(MIN_RETURN, (float, int)), tfi

    if DISCRETE_RETURNS:
        assert MIN_REWARD > 0
        assert MIN_RETURN > -1
    else:
        assert (
            MIN_REWARD > 1e-4
        ), "MIN_REWARD must be sufficiently large to prevent critic loss divergence"
        assert (
            MIN_RETURN > -1e38
        ), "MIN_RETURN must be greater than float32 (single-precision) limit -1e38"

    assert isinstance(MAX_RETURN, (float, int)), tfi
    assert MAX_RETURN > MIN_RETURN
    assert isinstance(MIN_WEIGHT, (float, int)), tfi
    assert MIN_WEIGHT > 0

    MIN_VALUE = max(MIN_VALUE_RATIO * INITIAL_VALUE, 1)
    MIN_VALUE_SPACE = min(
        MIN_VALUE / INITIAL_VALUE - 1, MIN_REWARD / MAX_VALUE, MIN_VALUE_RATIO
    )

    assert MAX_VALUE > MIN_VALUE
    assert INITIAL_PRICE > MIN_VALUE
    assert INITIAL_VALUE > MIN_VALUE
    assert MAX_VALUE_RATIO > MIN_VALUE_SPACE

    print("Test - {} Environments: Passed".format(name))


def coin_flip_env_tests(UP_PROB: float, UP_R: float, DOWN_R: float) -> None:
    """
    Conduct tests for coin flip environment initialisation.

    Parameters:
        Refer to `./envs/coin_flip_envs.py` for input details.
    """
    assert isinstance(UP_PROB, (float, int)), tfi
    assert 0 < UP_PROB < 1
    assert isinstance(UP_R, (float, int)), tfi
    assert UP_R > 0
    assert isinstance(DOWN_R, (float, int)), tfi
    assert DOWN_R < 0

    print("Test - Coin Flip Parameters: Passed")

    print(
        "--------------------------------------------------------------------------------"
    )


def dice_roll_env_tests(
    UP_PROB: float,
    DOWN_PROB: float,
    UP_R: float,
    DOWN_R: float,
    MID_R: float,
) -> None:
    """
    Conduct tests for dice roll environment initialisation.

    Parameters:
        Refer to `./envs/dice_roll_envs.py` for input details.
    """
    assert isinstance(UP_PROB, (float, int)), tfi
    assert 0 < UP_PROB < 1
    assert isinstance(DOWN_PROB, (float, int)), tfi
    assert 0 < DOWN_PROB < 1
    assert UP_PROB + DOWN_PROB < 1
    assert isinstance(UP_R, (float, int)), tfi
    assert UP_R > 0
    assert isinstance(DOWN_R, (float, int)), tfi
    assert DOWN_R < 0
    assert isinstance(MID_R, (float, int)), tfi
    assert UP_R > MID_R > DOWN_R

    print("Test - Dice Roll Parameter: Passed")

    print(
        "--------------------------------------------------------------------------------"
    )


def dice_roll_sh_env_tests(
    UP_PROB: float,
    DOWN_PROB: float,
    UP_R: float,
    DOWN_R: float,
    MID_R: float,
    SH_UP_R: float,
    SH_DOWN_R: float,
    SH_MID_R: float,
    I_LEV_FACTOR: float,
    SH_LEV_FACTOR: float,
) -> None:
    """
    Conduct tests for dice roll (safe haven) environment initialisation.

    Parameters:
        Refer to `./envs/dice_roll_sh_envs.py` for input details.
    """
    assert isinstance(UP_PROB, (float, int)), tfi
    assert 0 < UP_PROB < 1
    assert isinstance(DOWN_PROB, (float, int)), tfi
    assert 0 < DOWN_PROB < 1
    assert UP_PROB + DOWN_PROB < 1
    assert isinstance(UP_R, (float, int)), tfi
    assert UP_R > 0
    assert isinstance(DOWN_R, (float, int)), tfi
    assert DOWN_R < 0
    assert isinstance(MID_R, (float, int)), tfi
    assert UP_R > MID_R > DOWN_R

    assert isinstance(SH_UP_R, (float, int)), tfi
    assert -1 < SH_UP_R < 0
    assert isinstance(SH_DOWN_R, (float, int)), tfi
    assert SH_DOWN_R > 0
    assert isinstance(SH_MID_R, (float, int)), tfi
    assert SH_DOWN_R > SH_MID_R >= SH_UP_R

    assert I_LEV_FACTOR != 0
    assert isinstance(SH_LEV_FACTOR, (float, int)), tfi
    assert SH_LEV_FACTOR != 0

    print("Test - Dice Roll (Safe Haven) Parameters: Passed")

    print(
        "--------------------------------------------------------------------------------"
    )


def gbm_env_tests(DRIFT: float, VOL: float, LEV_FACTOR: float) -> None:
    """
    Conduct tests for GBM environment initialisation.

    Parameters:
        Refer to `./envs/gbm_envs.py` for input details.
    """
    assert isinstance(DRIFT, (float, int)), tfi
    assert isinstance(VOL, (float, int)), tfi
    assert VOL > 0, "volatility must be non-zero and non-negative"

    assert (
        DRIFT - VOL**2 / 2 > 0
    ), "require positive growth rate to achieve optimal (positive) leverage solution"

    assert isinstance(LEV_FACTOR, (float, int)), tfi
    assert LEV_FACTOR != 0, "must have non-zero leverage"

    print("Test - GBM Parameters: Passed")

    print(
        "--------------------------------------------------------------------------------"
    )


def market_env_tests(
    MAX_VALUE: float,
    INITIAL_VALUE: float,
    MIN_VALUE_RATIO: float,
    MAX_VALUE_RATIO: float,
    MAX_ABS_ACTION: float,
    MIN_REWARD: float,
    MIN_RETURN: float,
    MAX_RETURN: float,
    MIN_WEIGHT: float,
    LEV_FACTOR: float,
) -> None:
    """
    Conduct tests for market environment initialisation.

    Parameters:
        Refer to `./envs/market_envs.py` for input details.
    """
    assert isinstance(MAX_VALUE, (float, int)), tfi
    assert (
        0 < MAX_VALUE < 1e37
    ), "MAX_VALUE must be less than float32 (single-precision) limit 1e38"
    assert isinstance(INITIAL_VALUE, (float, int)), tfi
    assert MAX_VALUE > INITIAL_VALUE > 0
    assert isinstance(MIN_VALUE_RATIO, (float, int)), tfi
    assert 1 / MAX_VALUE <= MIN_VALUE_RATIO < 1
    assert isinstance(MAX_VALUE_RATIO, (float, int)), tfi
    assert MIN_VALUE_RATIO < MAX_VALUE_RATIO

    assert isinstance(MAX_ABS_ACTION, (float, int)), tfi
    assert MAX_ABS_ACTION > 0
    assert isinstance(MIN_REWARD, (float, int)), tfi
    assert MIN_REWARD > 0
    assert isinstance(MIN_RETURN, (float, int)), tfi
    assert MIN_RETURN > -1
    assert isinstance(MAX_RETURN, (float, int)), tfi
    assert MAX_RETURN > MIN_RETURN
    assert isinstance(MIN_WEIGHT, (float, int)), tfi
    assert MIN_WEIGHT > 0

    MIN_VALUE = max(MIN_VALUE_RATIO * INITIAL_VALUE, 1)
    MIN_OBS_SPACE = min(
        MIN_VALUE / INITIAL_VALUE - 1, MIN_REWARD / MAX_VALUE, MIN_VALUE
    )

    assert MAX_VALUE > MIN_VALUE
    assert INITIAL_VALUE > MIN_VALUE
    assert MAX_VALUE_RATIO > MIN_OBS_SPACE

    print("Test - Market Environments: Passed")

    assert isinstance(LEV_FACTOR, (float, int)), tfi
    assert LEV_FACTOR != 0

    print("Test - Market Parameters: Passed")

    print(
        "--------------------------------------------------------------------------------"
    )
