"""
Copyright (C) 2022 - 2024 J. S. Grewal <rg_public@proton.me>

Licence:                AGPL-3.0-or-later (https://www.gnu.org/licenses/agpl-3.0.en.html)

Codebase:               https://codeberg.org/raja-grewal

Title:                  resources_market.py

Description:
    Collection of tools required for various market environments.
"""

from typing import Tuple

import numpy as np
import numpy.typing as npt

NDArrayFloat = npt.NDArray[np.float_]


def market_dones(
    time: int,
    TIME_LENGTH: int,
    wealth: float,
    MIN_VALUE: float,
    reward: float,
    MIN_REWARD: float,
    step_return: float,
    MIN_RETURN: float,
    lev: NDArrayFloat,
    MIN_WEIGHT: float,
    MAX_ABS_ACTION: float,
    LEV_FACTOR: float,
    next_state: NDArrayFloat,
    MAX_VALUE_RATIO: float,
    active: float = None,
) -> Tuple[bool, bool]:
    """
    Agent done flags for multiplicative environments controlling episode termination
    and Q-value estimation for learning.

    Parameters:
        time: episode time step
        wealth: portfolio value
        reward: time-average growth rate
        step_return: single step return
        lev: asset leverages
        next_state: normalised state values
        active: bet size for investors B and C

    Returns:
        terminated: bool flag for episode terminal state
        truncated: bool flag for episode conclusion
    """
    done_time = time == TIME_LENGTH

    done_wealth = wealth == MIN_VALUE
    done_reward = reward < MIN_REWARD
    done_return = step_return == MIN_RETURN

    done_lev_max = np.all(np.abs(lev) == MAX_ABS_ACTION * LEV_FACTOR)
    done_lev_min = np.all(np.abs(lev) < MIN_WEIGHT)

    done_state = np.any(next_state >= MAX_VALUE_RATIO)

    done_active = False if active == None else active == 0

    truncated = bool(
        done_time
        or done_wealth
        or done_reward
        or done_return
        or done_lev_max
        or done_lev_min
        or done_state
        or done_active
    )

    terminated = truncated and not (done_time or done_state)

    return terminated, truncated


def observed_market_state(
    market_extract: NDArrayFloat, time_step: int, action_days: int, obs_days: int
) -> NDArrayFloat:
    """
    Flattened and ordered observed historical market prices used for agent decision-making.

    Parameters:
        market_extract: shuffled time series extract of history used for training/inference
        time_step: current time step in market_extract
        action_days: number of days between agent trading actions
        obs_days: number of previous days agent uses for decision-making

    Return:
        obs_state: current observed past states used for next decision
    """
    if obs_days == 1:
        return market_extract[time_step * action_days]

    if time_step > 0:
        return market_extract[
            time_step * action_days : time_step * action_days + obs_days
        ].reshape(-1)[::-1]
    else:
        return market_extract[time_step * action_days : obs_days].reshape(-1)[::-1]


def time_slice(
    prices: NDArrayFloat, extract_days: int, action_days: int, sample_days: int
) -> Tuple[NDArrayFloat, int]:
    """
    Extract sequential slice of time series preserving the non-i.i.d. nature of
    the data keeping heteroscedasticity and serial correlation relatively unchanged
    compared to random sampling.

    Parameters:
        prices: array of all assets prices across a shared time period
        extract_days: length of period to be extracted
        action_days: number of days between agent trading actions
        sample_days: maximum buffer of days restricting starting index

    Returns:
        market_extract: extracted time sliced data from complete time series
        start_idx: first index of sampled training time series
    """
    max_train = prices.shape[0] - sample_days

    start_idx = np.random.randint(0, max_train)
    end_idx = start_idx + extract_days * action_days + 1

    market_extract = prices[start_idx:end_idx]

    return market_extract, start_idx


def shuffle_data(prices: NDArrayFloat, interval_days: int) -> NDArrayFloat:
    """
    Split data into identical subset intervals and randomly shuffle data within each
    interval. Purpose is to generate a fairly random (non-parametric) bootstrap (or seed)
    for known historical data while preserving overall long-term trends and structure.

    Parameters:
        prices: array of all historical assets prices across a shared time period
        interval_days: size of ordered subsets

    Returns:
        shuffled_prices: prices randomly shuffled within each interval
    """
    length = prices.shape[0]
    mod = length % interval_days
    intervals = int((length - mod) / interval_days)

    shuffled_prices = np.empty((length, prices.shape[1]))

    if mod != 0:
        split_prices = np.split(prices[:-mod], indices_or_sections=intervals, axis=0)
    else:
        split_prices = np.split(prices, indices_or_sections=intervals, axis=0)

    for x in range(intervals):
        shuffled_prices[x * interval_days : (x + 1) * interval_days] = (
            np.random.permutation(split_prices[x])
        )

    if mod != 0:
        shuffled_prices[intervals * interval_days :] = np.random.permutation(
            prices[-mod:]
        )

    return shuffled_prices
