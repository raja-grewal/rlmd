"""
Copyright (C) 2022 - 2024 J. S. Grewal <rg_public@proton.me>

Licence:                AGPL-3.0-or-later (https://www.gnu.org/licenses/agpl-3.0.en.html)

Codebase:               https://codeberg.org/raja-grewal

Title:                  resources_multiplicative.py

Description:
    Collection of tools required for various multiplicative environments.
"""

from typing import Tuple

import numpy as np
import numpy.typing as npt

NDArrayFloat = npt.NDArray[np.float_]


def multi_dones(
    wealth: float,
    MIN_VALUE: float,
    reward: float,
    MIN_REWARD: float,
    step_return: float,
    MIN_RETURN: float,
    lev: NDArrayFloat,
    MIN_WEIGHT: float,
    MAX_ABS_ACTION: float,
    LEV_FACTOR: float,
    next_state: NDArrayFloat,
    MAX_VALUE_RATIO: float,
    active: float = None,
) -> Tuple[bool, bool]:
    """
    Agent done flags for multiplicative environments controlling episode termination
    and Q-value estimation for learning.

    Parameters:
        wealth: portfolio value
        reward: time-average growth rate
        step_return: single step return
        lev: asset leverages
        next_state: normalised state values
        active: bet size for investors B and C

    Returns:
        terminated: bool flag for episode terminal state
        truncated: bool flag for episode conclusion
    """
    done_wealth = wealth == MIN_VALUE
    done_reward = reward < MIN_REWARD
    done_return = step_return == MIN_RETURN

    done_lev_max = np.any(np.abs(lev) == MAX_ABS_ACTION * LEV_FACTOR)
    done_lev_min = np.all(np.abs(lev) < MIN_WEIGHT)

    done_state = np.any(next_state >= MAX_VALUE_RATIO)

    done_active = False if active == None else active == 0

    truncated = bool(
        done_wealth
        or done_reward
        or done_return
        or done_lev_max
        or done_lev_min
        or done_state
        or done_active
    )

    terminated = truncated and not done_state

    return terminated, truncated


def multi_gbm_dones(
    wealth: float,
    MIN_VALUE: float,
    reward: float,
    MIN_REWARD: float,
    step_return: float,
    MIN_RETURN: float,
    lev: NDArrayFloat,
    MIN_WEIGHT: float,
    MAX_ABS_ACTION: float,
    LEV_FACTOR: float,
    next_state: NDArrayFloat,
    MAX_VALUE_RATIO: float,
    active: float = None,
) -> Tuple[bool, bool]:
    """
    Agent done flags for multiplicative environments controlling episode termination
    and Q-value estimation for learning.

    Parameters:
        wealth: portfolio value
        reward: time-average growth rate
        step_return: single step return
        lev: asset leverages
        next_state: normalised state values
        active: bet size for investors B and C

    Returns:
        terminated: bool flag for episode terminal state
        truncated: bool flag for episode conclusion
    """
    done_wealth = wealth == MIN_VALUE
    done_reward = reward < MIN_REWARD
    done_return = step_return == MIN_RETURN

    done_lev_max = np.all(np.abs(lev) == MAX_ABS_ACTION * LEV_FACTOR)
    done_lev_min = np.all(np.abs(lev) < MIN_WEIGHT)

    done_state = np.any(next_state >= MAX_VALUE_RATIO)

    done_active = False if active == None else active == 0

    truncated = bool(
        done_wealth
        or done_reward
        or done_return
        or done_lev_max
        or done_lev_min
        or done_state
        or done_active
    )

    terminated = truncated and not done_state

    return terminated, truncated
