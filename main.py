"""
Copyright (C) 2022 - 2024 J. S. Grewal <rg_public@proton.me>

Licence:                AGPL-3.0-or-later (https://www.gnu.org/licenses/agpl-3.0.en.html)

Codebase:               https://codeberg.org/raja-grewal

Title:                  main.py
Usage:                  python main.py

Package Versioning:
    gymnasium           0.29
    imageio             2.34
    matplotlib          3.8
    mujoco              3.1
    scipy               1.12
    torch               2.2

Description:
    Responsible for executing all agent training and conducting tests on provided inputs.

Instructions:
    1. Select algorithms, critic loss functions, multi-steps, environments, and single
       asset labels, using available options and enter them into the provided
       first four lists (all must be non-empty).
    2. Modify inputs dictionary containing agent training/learning parameters and
       reinforcement learning model hyperparameters if required.
    3. Running python file will provide live progress of learning details in the terminal.
    4. Upon completion, all learned PyTorch parameters, data, and plots will be
       placed within the ./results/ directory. In all cases, these will be organised
       by the reward dynamic (additive or multiplicative or market). Inside each of
       these there will exist directories /data and /models directory containing
       sub-directories titled by full environment ID (env_id) containing all output
       data and summary plots.
"""

import time
from typing import Dict, List

# model-free off-policy (continuous action space) agents:
# ["SAC", "TD3"]
algo: List[str] = ["SAC"]

# critic loss functions:
# ["MSE", "HUB", "MAE", "HSC", "CAU", "TCAU", "CIM", "MSE2", "MSE4", "MSE6"]
critic: List[str] = ["MSE"]

# bootstrapping of target critic values and discounted rewards:
# [integer >0]
multi_steps: List[int] = [1]

# environments to train agent:
# integer ENV_KEYs from gym_envs dictionary below
envs: List[int] = [31]

# single asset labels for market envs to train agent:
# string LABELs from gym_envs dictionary below when using ENV_KEYs 31-33
single_asset_labels: List[str] = ["SPX"]

# fmt: off

gym_envs: Dict[str, list] = {
    # ENV_KEY: [env_id, state_dim, action_dim, initial warm-up steps to generate random seed]

    # ADDITIVE ENVIRONMENTS
        # MuJoCo environments (https://github.com/google-deepmind/mujoco)
            "0": ["InvertedPendulum-v4", 4, 1, 1e3],
            "1": ["Swimmer-v4", 8, 2, 1e3],
            "2": ["InvertedDoublePendulum-v4", 11, 1, 1e3],
            "3": ["Reacher-v4", 11, 2, 1e3],
            "4": ["Hopper-v4", 11, 3, 1e3],
            "5": ["Walker2d-v4", 17, 6, 1e3],
            "6": ["HalfCheetah-v4", 17, 6, 1e3],
            "7": ["Ant-v4", 27, 8, 1e3],
            "8": ["Humanoid-v4", 376, 17, 1e3],
            "9": ["HumanoidStandup-v4", 376, 17, 1e3],

    # MULTIPLICATIVE ENVIRONMENTS (MARKOV)
        # InvA = Investor A,  InvB = Investor B,  InvC = Investor C
        # assets following the binary coin flip
            "10": ["Coin_InvA", 5, 1, 1e3],
            "11": ["Coin_InvB", 5, 2, 1e3],
            "12": ["Coin_InvC", 5, 3, 1e3],
        # assets following the trinary dice roll
            "13": ["Dice_InvA", 5, 1, 1e3],
            "14": ["Dice_InvB", 5, 2, 1e3],
            "15": ["Dice_InvC", 5, 3, 1e3],
        # assets following GBM
            "16": ["GBM_InvA", 5, 1, 1e3],
            "17": ["GBM_InvB", 5, 2, 1e3],
            "18": ["GBM_InvC", 5, 3, 1e3],

    # MULTIPLICATIVE INSURANCE SAFE HAVEN ENVIRONMENTS (MARKOV)
        # single asset cost-effective risk mitigation with insurance
            "19": ["Dice_SH_INSURED", 6, 1, 1e3],
        # single asset following the dice roll with insurance safe havens
            "20": ["Dice_SH_InvA", 6, 2, 1e3],
            "21": ["Dice_SH_InvB", 6, 3, 1e3],
            "22": ["Dice_SH_InvC", 6, 4, 1e3],

    # MARKET ENVIRONMENTS (NON-MARKOV)
        # single asset LABELs (see https://codeberg.org/raja-grewal/stooq-commodities):
            # US-listed equity indices:
                # "SPX", "DJI", "NDX"
            # ^DJIA index components:
                # "AAPL-US", "AMGN-US", "AXP-US", "BA-US", "CAT-US", "CVX-US", "DIS-US", "HD-US", "IBM-US",
                # "INTC-US", "JNJ-US", "JPM-US", "KO-US", "MCD-US", "MMM-US", "MRK-US", "MSFT-US", "NKE-US",
                # "PFE-US", "PG-US", "VZ-US", "WBA-US", "WMT-US", "CSCO-US", "UNH-US"
            # Commodities (Minor | Medium | Major):
                # "GC-F", "SI-F", "CL-F" | "HG-F", "PL-F", "LS-F" | "PA-F", "RB-F", "LE-F", "KC-F", "OJ-F"
            "23": ["Single_InvA", 5, 1, 1e3],
            "24": ["Single_InvB", 5, 2, 1e3],
            "25": ["Single_InvC", 5, 3, 1e3],

        # multi-asset CFD portfolios
        # USEI: US-listed equity indices (^SPX, ^NDX, ^DJIA)
            "26": ["EI_InvA", 7, 3, 1e3],
            "27": ["EI_InvB", 7, 4, 1e3],
            "28": ["EI_InvC", 7, 5, 1e3],
        # Minor: USEI + Gold, Silver, WTI
            "29": ["Minor_InvA", 10, 6, 1e3],
            "30": ["Minor_InvB", 10, 7, 1e3],
            "31": ["Minor_InvC", 10, 8, 1e3],
        # Medium: Minor + Cooper, Platinum, Lumber
            "32": ["Medium_InvA", 13, 9, 1e3],
            "33": ["Medium_InvB", 13, 10, 1e3],
            "34": ["Medium_InvC", 13, 11, 1e3],
        # Major: Medium + Palladium, RBOB, Cattle, Coffee, OJ
            "35": ["Major_InvA", 18, 14, 1e3],
            "36": ["Major_InvB", 18, 15, 1e3],
            "37": ["Major_InvC", 18, 16, 1e3],
        # DJI: USEI + 25/30 Dow Jones (^DJIA) components
            "38": ["DJI_InvA", 33, 29, 1e3],
            "39": ["DJI_InvB", 33, 30, 1e3],
            "40": ["DJI_InvC", 33, 31, 1e3],
        # Full: Major + 26/30 Dow Jones (^DJIA) components
            "41": ["Full_InvA", 44, 40, 1e3],
            "42": ["Full_InvB", 44, 41, 1e3],
            "43": ["Full_InvC", 44, 42, 1e3],
    }

inputs: dict = {
    # LEARNING PARAMETERS
        # additive environment execution parameters
            "n_trials_add": 10,                 # number of training trials
            "n_cumsteps_add": 3e5,              # training steps per trial (must be greater than environment warm-up)
            "eval_freq_add": 1e3,               # interval (steps) between evaluation episodes
            "n_eval_add": 1e1,                  # number of evaluation episodes
            "max_eval_reward": 1e6,             # maximum score per evaluation episode
            "actor_percentile_add": 100,        # bottom percentile of actor mini-batch to be maximised (>0, <=100)
            "critic_percentile_add": 100,       # top percentile of critic mini-batch to be minimised (>0, <=100)

        # multiplicative environment execution parameters
            "n_trials_mul": 10,                 # ibid.
            "n_cumsteps_mul": 1e5,              # ibid.
            "eval_freq_mul": 1e3,               # ibid.
            "n_eval_mul": 1e3,                  # ibid.
            "max_eval_steps_mul": 250,          # maximum steps per evaluation episode
            "smoothing_window_mul": 2e3,        # training steps up to which action smoothing window is applied
            "actor_percentile_mul": 50,         # ibid.
            "critic_percentile_mul": 50,        # ibid.

            "n_gambles":                        # number of simultaneous gambles (List[int] >0)
                [1, 3, 5],

        # market environment execution parameters
            "market_dir":                       # directory containing historical market data
                "./tools/market_data/",

            "n_trials_mkt": 10,                 # ibid.
            "n_cumsteps_mkt": 3e5,              # ibid.
            "eval_freq_mkt": 1e3,               # ibid.
            "n_eval_mkt": 1e2,                  # ibid.
            "smoothing_window_mkt": 2e3,        # ibid.
            "actor_percentile_mkt": 50,         # ibid.
            "critic_percentile_mkt": 50,        # ibid.

            "action_days": 1,                   # number of days between agent trading actions (252 day years)
            "train_days": 1000,                 # length of each training period
            "test_days": 250,                   # length of each evaluation period
            "train_shuffle_days": 7,            # interval size (>=1) to be shuffled for training
            "test_shuffle_days": 5,             # interval size (>=1) to be shuffled for inference
            "gap_days_min": 5,                  # minimum spacing (>=0) between training and testing windows
            "gap_days_max": 120,                # maximum spacing (>=gap_days_min) between training-testing windows

            "past_days":                        # number of previous observed days (POMDP if =1) (List[int] >0)
                [1, 3, 5],

        # learning variables
            "gpu": "cuda:0",                    # CUDA-based GPU or use CPU ("cpu") for backpropagation
            "buffer_device": "cpu",             # RAM- or VRAM-based buffer (may be faster for 1-step, slower for multi-step)

            "buffer": 1e6,                      # maximum transitions in experience replay buffer
            "discount": 0.99,                   # discount factor for successive steps
            "trail": 50,                        # moving average of training episode scores used for model saving
            "cauchy_scale": 1,                  # Cauchy scale parameter initialisation value
            "r_abs_zero": None,                 # defined absolute zero value for rewards added to buffer
            "continue": False,                  # whether to continue learning with same parameters across trials

        # critic loss aggregation
            "critic_mean_type": "E",            # critic learning using either empirical "E" or shadow "S" means (only E)
            "shadow_low_mul": 1e0,              # lower bound multiplier of minimum for critic power law
            "shadow_high_mul": 1e1,             # upper bound multiplier of maximum for critic power law

    # MODEL HYPERPARAMETERS
        # SAC hyperparameters (https://arxiv.org/pdf/1812.05905.pdf)
            "sac_actor_learn_rate": 3e-4,       # actor learning rate (Adam optimiser)
            "sac_critic_learn_rate": 3e-4,      # critic learning rate (Adam optimiser)
            "sac_temp_learn_rate": 3e-4,        # log temperature learning rate (Adam optimiser)
            "sac_layer_1_units": 256,           # nodes in first fully connected layer
            "sac_layer_2_units": 256,           # nodes in second fully connected layer
            "sac_actor_step_update": 1,         # actor policy network update frequency (steps)
            "sac_temp_step_update": 1,          # temperature update frequency (steps)
            "sac_target_critic_update": 1,      # target critic networks update frequency (steps)
            "sac_target_update_rate": 5e-3,     # Polyak averaging rate for target network parameter updates

            "initial_logtemp": 0,               # initial log weighting given to entropy maximisation
            "reward_scale": 1,                  # constant scaling factor of next reward ("inverse temperature")
            "log_scale_min": -20,               # minimum log scale for stochastic policy
            "log_scale_max": 2,                 # maximum log scale for stochastic policy
            "reparam_noise": 1e-6,              # miniscule constant to keep logarithm of actions bounded

        # TD3 hyperparameters (https://arxiv.org/pdf/1802.09477.pdf)
            "td3_actor_learn_rate": 1e-3,       # ibid.
            "td3_critic_learn_rate": 1e-3,      # ibid.
            "td3_layer_1_units": 400,           # ibid.
            "td3_layer_2_units": 300,           # ibid.
            "td3_actor_step_update": 2,         # ibid.
            "td3_target_actor_update": 2,       # target actor network update frequency (steps)
            "td3_target_critic_update": 2,      # ibid.
            "td3_target_update_rate": 5e-3,     # ibid.

            "policy_noise": 0.1,                # exploration noise added to next actions
            "target_policy_noise": 0.2,         # noise added to next target actions acting as a regulariser
            "target_policy_clip": 0.5,          # clipping limit of noise added to next target actions

        # shared parameters
            "sample_dist": {                    # policy distribution (normal "N", Laplace "L", or multi-variate normal "MVN")
                "SAC": "MVN",
                "TD3": "N",
                },
            "batch_size": {                     # mini-batch size for actor-critic neural networks
                "SAC": 256,
                "TD3": 100,
                },
            "grad_step": {                      # standard gradient update frequency (steps)
                "SAC": 1,
                "TD3": 1,
                },
            "log_noise": 1e-6,                  # miniscule constant to keep tail estimation logarithm bounded
    }

# fmt: on

if __name__ == "__main__":
    from tests.test_input_agent import algo_method_checks, env_tests, learning_tests
    from tools.utils import (
        device_details,
        env_dynamics,
        input_initialisation,
        load_market_data,
    )

    inputs = input_initialisation(
        inputs,
        algo,
        critic,
        multi_steps,
        envs,
        single_asset_labels,
    )

    # CONDUCT TESTS
    learning_tests(inputs)  # learning algorithm checks
    env_tests(gym_envs, inputs)  # environment setup tests
    algo_method_checks()  # class method existence checks

    device_details(inputs)

    # IMPORT SCRIPTS
    from scripts.rl_additive import additive_env
    from scripts.rl_market import market_env
    from scripts.rl_multiplicative import multiplicative_env

    # obtain environment key limits based on reward dynamics
    (first_multi_key, first_safe_key, first_market_key, market_env_keys) = env_dynamics(
        gym_envs
    )

    start_time = time.perf_counter()

    for env_key in envs:
        begin_time = time.perf_counter()

        inputs["ENV_KEY"] = env_key

        # additive environments
        if env_key < first_multi_key:
            additive_env(gym_envs, inputs)

        # multiplicative environments
        elif env_key < first_safe_key:
            for gambles in inputs["n_gambles"]:
                multiplicative_env(gym_envs, inputs, n_gambles=gambles)

        # multiplicative insurance safe haven environments
        elif env_key < first_market_key:
            multiplicative_env(gym_envs, inputs, n_gambles=1)

        # market environments
        elif env_key <= market_env_keys[0]:
            for label in inputs["asset_labels"]:
                data = load_market_data(env_key, market_env_keys, inputs, label)

                for days in inputs["past_days"]:
                    market_env(
                        gym_envs,
                        inputs,
                        market_data=data,
                        obs_days=days,
                        asset_label=label,
                    )

        elif env_key <= market_env_keys[6]:
            data = load_market_data(env_key, market_env_keys, inputs)

            for days in inputs["past_days"]:
                market_env(gym_envs, inputs, market_data=data, obs_days=days)

        finish_time = time.perf_counter()
        env_time = finish_time - begin_time

        print(
            "ENV_KEY {} TIME: {:1.0f} seconds = {:1.1f} minutes = {:1.2f} hours".format(
                env_key, env_time, env_time / 60, env_time / 3600
            )
        )

    end_time = time.perf_counter()

    total_time = end_time - start_time

    print(
        "TOTAL TIME: {:1.0f} seconds = {:1.1f} minutes = {:1.2f} hours".format(
            total_time, total_time / 60, total_time / 3600
        )
    )
