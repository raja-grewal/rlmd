"""
Copyright (C) 2022 - 2024 J. S. Grewal <rg_public@proton.me>

Licence:                AGPL-3.0-or-later (https://www.gnu.org/licenses/agpl-3.0.en.html)

Codebase:               https://codeberg.org/raja-grewal

Title:                  plot_msic.py

Description:
    Plotting of all final miscellaneous figures excluding agent training.
"""

import sys

sys.path.append("./")

import os
from os import PathLike
from typing import Union

import matplotlib.pyplot as plt
import numpy as np
import numpy.typing as npt
import pandas as pd
import torch as T
from scipy.stats import norm

NDArrayFloat = npt.NDArray[np.float_]

import plotting.plots_multiverse as lev_plots
import tools.critic_loss as closs
import tools.utils as utils


def leverage_plots(
    data_path: Union[str, bytes, PathLike], path: Union[str, bytes, PathLike]
) -> None:
    """
    Plots all leverage experiment results for all relevant investors.

    Parameters:
        data_path: location of leverage experiment data
        path: location to save figures
    """
    # leverage: coin flip
    inv4_lev_data = np.load(data_path + "coin_inv4_lev.npy")
    lev_plots.plot_inv4(inv4_lev_data, path + "coin_inv4")

    inv3_val_data = np.load(data_path + "coin_inv3_val.npy")
    lev_plots.plot_inv3(inv3_val_data, path + "coin_inv3")

    inv2_val_data = np.load(data_path + "coin_inv2_val.npy")
    lev_plots.plot_inv2(inv2_val_data, 30, path + "coin_inv2")

    inv1_val_data = np.load(data_path + "coin_inv1_val.npy")
    inv1_val_data_T = np.load(data_path + "coin_inv1_val_T.npy")
    lev_plots.plot_inv1(inv1_val_data, inv1_val_data_T, 1e30, path + "coin_inv1")

    # leverage: dice roll
    inv1_val_data = np.load(data_path + "dice_inv1_val.npy")
    inv1_val_data_T = np.load(data_path + "dice_inv1_val_T.npy")
    lev_plots.plot_inv1(inv1_val_data, inv1_val_data_T, 1e40, path + "dice_inv1")

    inv2_val_data = np.load(data_path + "dice_inv2_val.npy")
    lev_plots.plot_inv2(inv2_val_data, 100, path + "dice_inv2")

    inv3_val_data = np.load(data_path + "dice_inv3_val.npy")
    lev_plots.plot_inv3(inv3_val_data, path + "dice_inv3")

    # leverage: GBM
    from lev.gbm import name

    for x in range(0, len(name)):
        inv1_val_data = np.load(data_path + name[x] + "_inv1_val.npy")
        inv1_val_data_T = np.load(data_path + name[x] + "_inv1_val_T.npy")
        lev_plots.plot_inv1(
            inv1_val_data,
            inv1_val_data_T,
            1e40,
            path + name[x] + "_inv1",
        )

    # leverage: dice roll with insurance safe haven
    inv1_val_data = np.load(data_path + "dice_sh_inv1_val.npy")
    inv1_val_data_T = np.load(data_path + "dice_sh_inv1_val_T.npy")
    lev_plots.plot_inv1(inv1_val_data, inv1_val_data_T, 1e40, path + "dice_sh_inv1")


def loss_fn_plot(filename: Union[str, bytes, PathLike]) -> None:
    """
    Plot of critic loss functions about the origin.

    Parameters:
        filename: save path of plot
    """
    pdf = T.distributions.normal.Normal(0, 10)
    q = int(1e6)

    a = pdf.sample((q,))
    b = pdf.sample((q,))
    c = b - a

    mse = closs.mse(a, b, 0)
    mse2 = closs.mse(a, b, 2)
    mse4 = closs.mse(a, b, 4)
    huber = closs.huber(a, b)
    mae = closs.mae(a, b)
    hsc = closs.hypersurface(a, b)
    cauchy = closs.cauchy(a, b, 1)

    size = 3
    cols = ["C" + str(x) for x in range(7)]
    l = ["MSE", "MSE2", "MSE4", "Huber", "MAE", "HSC", "Cauchy"]

    plt.scatter(c, mse, s=size, c=cols[0], rasterized=True)
    plt.scatter(c, mse2, s=size, c=cols[1], rasterized=True)
    plt.scatter(c, mse4, s=size, c=cols[2], rasterized=True)
    plt.scatter(c, huber, s=size, c=cols[3], rasterized=True)
    plt.scatter(c, mae, s=size, c=cols[4], rasterized=True)
    plt.scatter(c, hsc, s=size, c=cols[5], rasterized=True)
    plt.scatter(c, cauchy, s=size, c=cols[6], rasterized=True)

    plt.xlim((-6, 6))
    plt.ylim((0, 5))
    plt.tick_params(axis="both", which="major", labelsize="small")
    plt.title("Loss", size="large")
    plt.legend(
        l, loc="lower right", ncol=1, frameon=False, fontsize="medium", markerscale=6
    )
    plt.tight_layout()

    plt.savefig(filename + ".png", dpi=400, format="png")
    plt.savefig(filename + ".svg", dpi=400, format="svg")

    plt.close()


def plot_smoothing_fn(filename: Union[str, bytes, PathLike]) -> None:
    """
    Plot action smoothing function for multiplicative and market environments.

    Parameters:
        filename: save path of plot
    """
    ratio = np.linspace(0, 1, 10000)
    f = utils.smoothing_func(ratio)

    plt.plot(ratio, f)
    plt.xlabel("Ratio " + r"$t / T_s$")
    plt.ylabel("Action Smoothing " + r"$s_t$")
    plt.grid(True, linewidth=0.5)

    plt.savefig(filename + ".png", dpi=400, format="png")
    plt.savefig(filename + ".svg", format="svg")

    plt.close()


def plot_gbm_max_lev(filename: Union[str, bytes, PathLike]) -> None:
    """
    Plot of GBM maximum leverage across down sigma moves.

    Parameters:
        filename: save path of plot
    """
    # S&P 500 parameters
    mu = 0.0540025395205692
    sigma = 0.1897916175617430
    l_opt = mu / sigma**2

    v0 = [2, 3, 5, 10]
    vmin = 1

    sig = np.linspace(1, 10, 1000)
    prob = norm.pdf(sig, 0, 1)
    prob = np.log10(prob)

    rho = np.linspace(1, 10, 1000)

    l_ratio = np.log(v0) - np.log(vmin)
    l_factor = 2 / (sigma * (2 * rho + sigma) - 2 * mu)

    fig = plt.figure()
    ax1 = fig.add_subplot(1, 1, 1, label="lev")
    ax2 = fig.add_subplot(1, 1, 1, label="prob", frame_on=False)

    l_eff = []
    for x in range(len(v0)):
        l_eff.append(l_factor * l_ratio[x])
        ax1.plot(rho, l_eff[x], color="C" + str(x + 1))

    ax1.set_xlabel(r"$\rho_d$")
    ax1.yaxis.tick_left()
    ax1.set_ylabel("Maximum Leverage")
    ax1.yaxis.set_label_position("left")
    ax1.tick_params(axis="y")
    ax1.grid(True, linewidth=0.5)

    ax2.plot(sig, prob, color="C0")
    ax2.axes.get_xaxis().set_visible(False)
    ax2.yaxis.tick_right()
    ax2.set_ylabel("Probability (log" + r"$_{10}$" + ")", color="C0")
    ax2.yaxis.set_label_position("right")
    ax2.tick_params(axis="y", colors="C0")

    ax1.hlines(l_opt, 1, 10, colors="black", linestyle="--")

    fig.subplots_adjust(hspace=0.3)
    fig.legend(
        v0,
        loc="upper center",
        ncol=len(v0),
        frameon=False,
        fontsize="medium",
        title=r"$V_{t-1}/V_{\min}$",
        title_fontsize="medium",
    )

    plt.savefig(filename + ".png", dpi=400, format="png")
    plt.savefig(filename + ".svg", format="svg")

    plt.close()


def plot_shuffled_histories(filename: Union[str, bytes, PathLike]) -> None:
    """
    Plot of unique histories as a function of shuffled days.

    Parameters:
        filename: save path of plot
    """
    days, train, eval = [], [], []

    n = 9000
    mt = 1000
    me = 250
    g = 120

    for x in range(1, 16, 1):
        dt, de = x, x
        ut, ue = utils.unique_histories(n, mt, me, g, dt, de)
        days.append(x)
        train.append(ut)
        eval.append(ue)

    plt.plot(days, train, color="C0")
    plt.plot(days, eval, color="C1")
    plt.xlabel("Shuffled Days " + r"$d_k$")
    plt.ylabel("Unique Histories " + r"$u_k$" + " (log" + r"$_{10}$" + ")")
    plt.grid(True, linewidth=0.5)

    l = ["Training", "Evaluation"]
    plt.legend(
        l,
        loc="upper center",
        ncol=2,
        frameon=False,
        fontsize="large",
        markerscale=6,
        bbox_to_anchor=(0.5, 1.15),
    )

    plt.savefig(filename + ".png", dpi=400, format="png")
    plt.savefig(filename + ".svg", format="svg")

    plt.close()


def market_prices(
    data_path: Union[str, bytes, PathLike],
    file_path: Union[str, bytes, PathLike],
) -> None:
    """
    Plot market prices of several assets using daily close prices.

    Parameters:
        path_data: location of market data
        filename: save path of plot
    """
    if os.path.isfile(data_path + "stooq_usei.pkl"):
        usei = pd.read_pickle(data_path + "stooq_usei.pkl")

        usei = usei["Close"].dropna()
        usei = usei[::-1] if usei.index[0] > usei.index[-1] else usei
        usei_x = np.linspace(1, len(usei["^SPX"]), 10)

        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax2 = ax1.twiny()

        for x in ["^SPX", "^NDX", "^DJI"]:
            ax1.plot(usei[x], linewidth=0.75, rasterized=True)

        ax1.grid(True, linewidth=0.2)
        ax1.margins(x=0)

        ax1.set_ylabel("Value")
        ax1.set_xlabel("Year")

        ax2.xaxis.set_ticks_position("bottom")
        ax2.xaxis.set_label_position("bottom")
        ax2.spines["bottom"].set_position(("axes", -0.15))
        ax2.set_frame_on(True)
        ax2.patch.set_visible(False)
        for sp in ax2.spines.values():
            sp.set_visible(False)
        ax2.spines["bottom"].set_visible(True)

        ax2.set_xticks(usei_x)
        ax2.set_xlabel("Count " + r"$N$" + " (Days)")

        l = ["S&P 500", "Nasdaq", "Dow Jones"]
        leg = ax1.legend(
            l, loc="upper left", ncol=1, frameon=False, fontsize="medium", markerscale=3
        )

        leg_lines = leg.get_lines()
        plt.setp(leg_lines, linewidth=3)

        filename = file_path + "mar_" + "usei"
        plt.savefig(
            filename + ".png",
            dpi=400,
            format="png",
            pad_inches=0.2,
            bbox_inches="tight",
        )
        plt.savefig(
            filename + ".svg",
            dpi=300,
            format="svg",
            pad_inches=0.2,
            bbox_inches="tight",
        )

        plt.close()

        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax2 = ax1.twiny()

        for x in ["^SPX", "^NDX", "^DJI"]:
            ax1.plot(np.log10(usei[x]), linewidth=0.75, rasterized=True)

        ax1.grid(True, linewidth=0.2)
        ax1.margins(x=0)

        ax1.set_ylabel("Value (log" + r"$_{10}$" + ")")
        ax1.set_xlabel("Year")

        ax2.xaxis.set_ticks_position("bottom")
        ax2.xaxis.set_label_position("bottom")
        ax2.spines["bottom"].set_position(("axes", -0.15))
        ax2.set_frame_on(True)
        ax2.patch.set_visible(False)
        for sp in ax2.spines.values():
            sp.set_visible(False)
        ax2.spines["bottom"].set_visible(True)

        ax2.set_xticks(usei_x)
        ax2.set_xlabel("Count " + r"$N$" + " (Days)")

        leg_lines = leg.get_lines()
        plt.setp(leg_lines, linewidth=3)

        filename = file_path + "mar_" + "usei" + "_log"
        plt.savefig(
            filename + ".png",
            dpi=400,
            format="png",
            pad_inches=0.2,
            bbox_inches="tight",
        )
        plt.savefig(
            filename + ".svg",
            dpi=300,
            format="svg",
            pad_inches=0.2,
            bbox_inches="tight",
        )

        plt.close()

    else:
        print(
            "Missing {} for USEI time series plot.".format(data_path + "stooq_usei.pkl")
        )

    if os.path.isfile(data_path + "stooq_dji.pkl"):
        dji = pd.read_pickle(data_path + "stooq_dji.pkl")

        dji = dji["Close"].dropna()
        dji = dji[::-1] if dji.index[0] > dji.index[-1] else dji
        dji_x = np.linspace(1, len(dji["^SPX"]), 10)

        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax2 = ax1.twiny()

        for x in [
            "AAPL.US",
            "AMGN.US",
            "AXP.US",
            "BA.US",
            "CAT.US",
            "CVX.US",
            "DIS.US",
            "HD.US",
            "IBM.US",
            "INTC.US",
            "JNJ.US",
            "JPM.US",
            "KO.US",
            "MCD.US",
            "MMM.US",
            "MRK.US",
            "MSFT.US",
            "NKE.US",
            "PFE.US",
            "PG.US",
            "VZ.US",
            "WBA.US",
            "WMT.US",
            "CSCO.US",
            "UNH.US",
        ]:
            ax1.plot(dji[x], linewidth=0.75, rasterized=True)

        ax1.grid(True, linewidth=0.2)
        ax1.margins(x=0)

        ax1.set_ylabel("Value")
        ax1.set_xlabel("Year")

        ax2.xaxis.set_ticks_position("bottom")
        ax2.xaxis.set_label_position("bottom")
        ax2.spines["bottom"].set_position(("axes", -0.15))
        ax2.set_frame_on(True)
        ax2.patch.set_visible(False)
        for sp in ax2.spines.values():
            sp.set_visible(False)
        ax2.spines["bottom"].set_visible(True)

        ax2.set_xticks(dji_x)
        ax2.set_xlabel("Count " + r"$N$" + " (Days)")

        l = [
            "AAPL",
            "AMGN",
            "AXP",
            "BA",
            "CAT",
            "CVX",
            "DIS",
            "HD",
            "IBM",
            "INTC",
            "JNJ",
            "JPM",
            "KO",
            "MCD",
            "MMM",
            "MRK",
            "MSFT",
            "NKE",
            "PFE",
            "PG",
            "VZ",
            "WBA",
            "WMT",
            "CSCO",
            "UNH",
        ]
        leg = ax1.legend(l, loc="upper left", ncol=2, frameon=False, fontsize="medium")

        leg_lines = leg.get_lines()
        plt.setp(leg_lines, linewidth=3)

        filename = file_path + "mar_" + "dji"
        plt.savefig(
            filename + ".png",
            dpi=200,
            format="png",
            pad_inches=0.2,
            bbox_inches="tight",
        )
        plt.savefig(
            filename + ".svg",
            dpi=300,
            format="svg",
            pad_inches=0.2,
            bbox_inches="tight",
        )

        plt.close()

        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax2 = ax1.twiny()

        for x in [
            "AAPL.US",
            "AMGN.US",
            "AXP.US",
            "BA.US",
            "CAT.US",
            "CVX.US",
            "DIS.US",
            "HD.US",
            "IBM.US",
            "INTC.US",
            "JNJ.US",
            "JPM.US",
            "KO.US",
            "MCD.US",
            "MMM.US",
            "MRK.US",
            "MSFT.US",
            "NKE.US",
            "PFE.US",
            "PG.US",
            "VZ.US",
            "WBA.US",
            "WMT.US",
            "CSCO.US",
            "UNH.US",
        ]:
            ax1.plot(np.log10(dji[x]), linewidth=0.75, rasterized=True)

        ax1.grid(True, linewidth=0.2)
        ax1.margins(x=0)

        ax1.set_ylabel("Value (log" + r"$_{10}$" + ")")
        ax1.set_xlabel("Year")

        ax2.xaxis.set_ticks_position("bottom")
        ax2.xaxis.set_label_position("bottom")
        ax2.spines["bottom"].set_position(("axes", -0.15))
        ax2.set_frame_on(True)
        ax2.patch.set_visible(False)
        for sp in ax2.spines.values():
            sp.set_visible(False)
        ax2.spines["bottom"].set_visible(True)

        ax2.set_xticks(dji_x)
        ax2.set_xlabel("Count " + r"$N$" + " (Days)")

        leg_lines = leg.get_lines()
        plt.setp(leg_lines, linewidth=3)

        filename = file_path + "mar_" + "dji" + "_log"
        plt.savefig(
            filename + ".png",
            dpi=200,
            format="png",
            pad_inches=0.2,
            bbox_inches="tight",
        )
        plt.savefig(
            filename + ".svg",
            dpi=300,
            format="svg",
            pad_inches=0.2,
            bbox_inches="tight",
        )

        plt.close()

    else:
        print(
            "Missing {} for DJI time series plot.".format(data_path + "stooq_dji.pkl")
        )

    if os.path.isfile(data_path + "stooq_major.pkl"):
        major = pd.read_pickle(data_path + "stooq_major.pkl")

        major = major["Close"].dropna()
        major = major[::-1] if major.index[0] > major.index[-1] else major
        major_x = np.linspace(1, len(major["^SPX"]), 10)

        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax2 = ax1.twiny()

        for x in [
            "GC.F",
            "SI.F",
            "CL.F",
            "HG.F",
            "PL.F",
            "LS.F",
            "PA.F",
            "RB.F",
            "LE.F",
            "KC.F",
            "OJ.F",
        ]:
            ax1.plot(major[x], linewidth=0.75, rasterized=True)

        ax1.grid(True, linewidth=0.2)
        ax1.margins(x=0)

        ax1.set_ylabel("Value (log" + r"$_{10}$" + ")")
        ax1.set_xlabel("Year")

        ax2.xaxis.set_ticks_position("bottom")
        ax2.xaxis.set_label_position("bottom")
        ax2.spines["bottom"].set_position(("axes", -0.15))
        ax2.set_frame_on(True)
        ax2.patch.set_visible(False)
        for sp in ax2.spines.values():
            sp.set_visible(False)
        ax2.spines["bottom"].set_visible(True)

        ax2.set_xticks(major_x)
        ax2.set_xlabel("Count " + r"$N$" + " (Days)")

        l = [
            "Gold",
            "Silver",
            "Crude Oil WTI",
            "High Grade Copper",
            "Platinum",
            "Lumber Random Length",
            "Palladium",
            "RBOB Gasoline",
            "Live Cattle",
            "Coffee",
            "Orange Juice",
        ]
        leg = ax1.legend(l, loc="upper left", ncol=1, frameon=False, fontsize="medium")

        leg_lines = leg.get_lines()
        plt.setp(leg_lines, linewidth=3)

        filename = file_path + "mar_" + "major"
        plt.savefig(
            filename + ".png",
            dpi=200,
            format="png",
            pad_inches=0.2,
            bbox_inches="tight",
        )
        plt.savefig(
            filename + ".svg",
            dpi=300,
            format="svg",
            pad_inches=0.2,
            bbox_inches="tight",
        )

        plt.close()

        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax2 = ax1.twiny()

        for x in [
            "GC.F",
            "SI.F",
            "HG.F",
            "PL.F",
            "CL.F",
            "LS.F",
            "PA.F",
            "RB.F",
            "LE.F",
            "KC.F",
            "OJ.F",
        ]:
            ax1.plot(np.log10(major[x]), linewidth=0.75, rasterized=True)

        ax1.grid(True, linewidth=0.2)
        ax1.margins(x=0)

        ax1.set_ylabel("Value (log" + r"$_{10}$" + ")")
        ax1.set_xlabel("Year")

        ax2.xaxis.set_ticks_position("bottom")
        ax2.xaxis.set_label_position("bottom")
        ax2.spines["bottom"].set_position(("axes", -0.15))
        ax2.set_frame_on(True)
        ax2.patch.set_visible(False)
        for sp in ax2.spines.values():
            sp.set_visible(False)
        ax2.spines["bottom"].set_visible(True)

        ax2.set_xticks(major_x)
        ax2.set_xlabel("Count " + r"$N$" + " (Days)")

        leg_lines = leg.get_lines()
        plt.setp(leg_lines, linewidth=3)

        filename = file_path + "mar_" + "major" + "_log"
        plt.savefig(
            filename + ".png",
            dpi=200,
            format="png",
            pad_inches=0.2,
            bbox_inches="tight",
        )
        plt.savefig(
            filename + ".svg",
            dpi=300,
            format="svg",
            pad_inches=0.2,
            bbox_inches="tight",
        )

        plt.close()

    else:
        print(
            "Missing {} for commodities time series plot.".format(
                data_path + "stooq_major.pkl"
            )
        )
